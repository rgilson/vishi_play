package br.com.vishi.vishiplay.features.movies_list

import androidx.recyclerview.widget.RecyclerView
import br.com.vishi.vishiplay.databinding.MovieItemBinding
import br.com.vishi.vishiplay.model.Movie

class MovieViewHolder(private val binding: MovieItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(movie: Movie) {
        binding.model = movie
    }
}