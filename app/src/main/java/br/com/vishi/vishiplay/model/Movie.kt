package br.com.vishi.vishiplay.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    var id: String? = null,
    var type: String? = "",
    var website: String? = "",
    var title: String? = "",
    var released: String? = "",
    var runtime: String? = "",
    var genre: String? = "",
    var plot: String? = "",
    var awards: String? = "",
    var poster: String? = "",
    var year:Long? = 0
) : Parcelable