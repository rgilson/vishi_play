package br.com.vishi.vishiplay.features.movie_detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_movie_detail.*
import android.view.MenuItem
import br.com.vishi.vishiplay.R
import br.com.vishi.vishiplay.model.Movie
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class MovieDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        val movie = intent?.getParcelableExtra<Movie>("movie")
        if (movie!=null) {

            Glide.with(this)
                .load(movie.poster)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.loading_animations)
                        .error(R.drawable.cartaz))
                .into(dtFoto)

            tv_title.setText( movie.title)
            tv_votos.setText("Duração: "+movie.runtime)
            tv_lancamento.setText("Lançamento: "+movie.released)
            tvSinopse.setText(movie.awards)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
