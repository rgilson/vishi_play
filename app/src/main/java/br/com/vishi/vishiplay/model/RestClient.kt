package br.com.vishi.vishiplay.model


import android.util.Log
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*


interface RestClient {

    @GET("filmes.json")
    fun movies(): Observable<HashMap<String, Movie>>

    @POST("movies.json")
    fun post(@Body movie: Movie): Observable<ResponseBody>

    @PUT("movies.json")
    fun put(@Body movie: HashMap<String, Movie>): Observable<HashMap<String, Movie>>

    @DELETE("movies/{movieId}.json")
    fun delete(@Path("movieId") movieId: String): Observable<ResponseBody>

    object Factory {

        fun create(): RestClient {

            val BASE_URL = "https://vishiplay-59311.firebaseio.com/"

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

            Log.d("RETROFIT",BASE_URL)

            return retrofit.create(RestClient::class.java)
        }
    }
}