package br.com.vishi.vishiplay.features.movies_list

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.vishi.vishiplay.databinding.ActivityMoviesBinding
import br.com.vishi.vishiplay.model.Movie
import br.com.vishi.vishiplay.utils.PermissionUtils
import br.com.vishi.vishiplay.utils.isOnLine
import kotlinx.android.synthetic.main.activity_movies.*
import org.jetbrains.anko.toast
import android.content.Intent
import br.com.vishi.vishiplay.R
import br.com.vishi.vishiplay.features.movie_detail.MovieDetailActivity

class MoviesActivity : AppCompatActivity() {

    lateinit var binding: ActivityMoviesBinding
    lateinit var moviesAdapter: MoviesAdapter

    private val viewModel: MoviesViewModel by lazy {
        ViewModelProviders.of(this).get(MoviesViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= 21) {
            val permissions =
                packageManager.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS)
            PermissionUtils.validate(this, *permissions.requestedPermissions)
        }

        supportActionBar?.title = getString(R.string.app_name)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_movies)
        val decoration = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        moviesAdapter = MoviesAdapter(){
            val intent = Intent(this, MovieDetailActivity::class.java)
            intent.putExtra("movie",it)
            startActivity(intent)
        }

        binding.layoutManager = GridLayoutManager(this, 3)
        binding.dividerItemDecoration = decoration
        binding.adapter = moviesAdapter
        binding.viewModel = viewModel

        progressBar.visibility=View.VISIBLE

        if (isOnLine(this)) {
            viewModel.data.observe(
                this, Observer<MutableCollection<Movie>> { response ->
                    run {
                        Log.d("HomeFragment", response.toString())
                        moviesAdapter.setData(response)
                    }
                }
            )
            progressBar.visibility=View.GONE
            llLista.visibility = View.VISIBLE
            imgInternet.visibility = View.GONE
        } else {
            toast("VOCE ESTÁ SEM INTERNET")
            llLista.visibility = View.GONE
            imgInternet.visibility = View.VISIBLE
            progressBar.visibility=View.GONE
        }

    }

    fun confirmaSaida(activit: Activity): Boolean {
        val alertDialogBuilder = android.app.AlertDialog.Builder(this)
        val nome = getString(R.string.app_name)
        alertDialogBuilder.setMessage(getString(R.string.app_sair) + " $nome?")
        alertDialogBuilder.setPositiveButton(getString(R.string.app_sim)) { dialog, which ->
            activit.finish()
        }
        alertDialogBuilder.setNegativeButton(getString(R.string.app_nao), null)
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
        return true
    }

    override fun onBackPressed() {
        confirmaSaida(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            confirmaSaida(this)
        }
        return super.onOptionsItemSelected(item)
    }

}
