package br.com.vishi.vishiplay.features.movies_list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.vishi.vishiplay.model.Movie
import br.com.vishi.vishiplay.model.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MoviesViewModel : ViewModel() {

    lateinit var subscribe: Disposable
    private val client = RestClient.Factory.create()
    val data: MutableLiveData<MutableCollection<Movie>> = MutableLiveData()

    init {
        loadData()
    }

    fun loadData() {

        subscribe = client.movies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                data.postValue(response.values)
                Log.d("ERRO",response.toString())
            }, { error ->
                System.out.println(error.toString())
            })
    }

    override fun onCleared() {
        super.onCleared()
        subscribe.dispose()
    }
}