package br.com.vishi.vishiplay

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import br.com.vishi.vishiplay.features.movies_list.MoviesActivity

class SplashActivity : AppCompatActivity() {

    lateinit var anim: Animation
    lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //Declara a imagem
        imageView = findViewById<View>(R.id.imgLogo) as ImageView
        //Carrega a animação
        anim = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)

        //Carrega a activity principal
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                startActivity(Intent(this@SplashActivity, MoviesActivity::class.java))
                finish()
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        imageView.startAnimation(anim)

    }

    companion object {
        // Timer da splash screen
        private val SPLASH_TIME_OUT = 5000
    }
}